#include <iostream>
#include <map>

#include "generators.hpp"

using std::cin;
using std::cout;
using std::endl;
using std::string;


//#define SMALL_FLAT_MAP

int main(int argc, const char **argv)
{
#ifdef SMALL_FLAT_MAP 
    int map_size[3] = { 10, 10, 10 };
#else
    int map_size[3] = { 70, 40, 10 };
#endif

    //alloc map arrays
    int ***map_data = new int**[map_size[0]];
    for (size_t x = 0; x < map_size[0]; x++) {
        map_data[x] = new int*[map_size[1]];
        for (size_t y = 0; y < map_size[1]; y++) {
            map_data[x][y] = new int[map_size[2]];
                for (size_t z = 0; z < map_size[2]; z++)
                    map_data[x][y][z] = false;
        }
    }

#ifdef SMALL_FLAT_MAP
    for (int x = 0; x < map_size[0]; x++)
        for (int y = 0; y < map_size[1]; y++)
            for (int z = 0; z < 3; z++)
                map_data[x][y][z] = true;
#else
    // fill map with test data
    size_t ground_z = 5;
    for (int x = 0; x < map_size[0]; x++)
        for (int y = 0; y < map_size[1]; y++)
            for (int z = 0; z <= ground_z; z++)
                map_data[x][y][z] = true;
    for (int x = 30; x < 40; x++)
        for (int y = 10; y < 20; y++)
            for (int z = ground_z; z < ground_z+1; z++)
                map_data[x][y][z] = true;
    for (int x = 32; x < 38; x++)
        for (int y = 12; y < 18; y++)
            for (int z = ground_z+1; z < ground_z+2; z++)
                map_data[x][y][z] = true;
    for (int x = 10; x < map_size[0]; x++)
        for (int y = 20; y < 25; y++)
            for (int z = ground_z-1; z <= ground_z; z++)
                map_data[x][y][z] = false;
#endif

    std::map<string, string> options = {
        { "rain intensity", "0.1" }
    };
    if (argc == 2) {
        options["iterations"] = argv[1];
    }
    genWater(map_size, map_data, options);

    // output map layers
    cout << map_size[0] << ' ' << map_size[1] << ' ' << map_size[2] << endl;
    for (int z = 0; z < map_size[2]; z++) {
        for (int y = 0; y < map_size[1]; y++) {
            for (int x = 0; x < map_size[0]; x++) {
                cout << (map_data[x][y][z] ? '#' : '.');
            }
            cout << '\n';
        }
        cout << '\n';
    }
    return 0;
}
