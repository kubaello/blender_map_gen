#include <cassert>
#include <list>
#include <map>
#include <random>
#include <unordered_set>

#include <boost/lexical_cast.hpp>
#include <boost/multi_array.hpp>

#include "generators.hpp"
#include "voxel_map.hpp"


namespace voxel1 {

enum is_flow_possible_e {
    flow_possible,
    other_too_high,
    other_too_low,
};

class top_voxel_data {
public:
    struct sink_flag {};

    top_voxel_data(size_t this_index, vec3<int> position, double air_above=std::numeric_limits<double>::infinity())
        : this_index(this_index)
        , position(position)
        , max_content_sum(double(position[2]) + 1.0 + air_above)
        , mud_level(0.0)
        , clear_water(0.0)
        , floating_mud(0.0)
        , momentum({ 0.0, 0.0, 0.0 })
        , has_rainfall(false)
        , _is_sink(false)
    {
    }
    top_voxel_data(sink_flag, size_t this_index, int x, int y)
        : this_index(this_index)
        , position(vec3<int>({ x, y, -100 }))
        , max_content_sum(std::numeric_limits<double>::infinity())
        , mud_level(0.0)
        , clear_water(0.0)
        , floating_mud(0.0)
        , momentum( 0.0, 0.0, 0.0 )
        , has_rainfall(false)
        , _is_sink(true)
    {
    }
    size_t this_index;
    std::unordered_set<size_t> related_indices;
    vec3<int> position; //ground voxel position
    double max_content_sum; //max total (z + mud + clear water + floating mud), limited by a voxel above
    double mud_level; //height of mud accumulated on this voxel, 1.0=full new voxel
    double clear_water; //current clear water on this voxel (doesn't include floating mud)
    double floating_mud; //mud in water, higher -> higher sedimentation
    vec3<double> momentum;

    bool has_rainfall; //if true, rain falls on this voxel (is a source of water)
    bool _is_sink; //if true, act as water sink (used as neighbors for map's edge voxels)

    double total_water_level() const {
        return double(position[2] + mud_level + clear_water + floating_mud);
    }
    double total_liquid_amount() const {
        return clear_water + floating_mud;
    }

    struct liquid {
        double clear_water;
        double floating_mud;
        vec3<double> momentum;
    };
    liquid take_liquid(double amount) {
        const double fract = amount / total_liquid_amount();
        const double clear_water_taken = fract*clear_water;
        const double floating_mud_taken = fract*floating_mud;
        const vec3<double> momentum_taken = momentum*fract;
        clear_water -= clear_water_taken;
        floating_mud -= floating_mud_taken;
        momentum -= momentum_taken;
        return{ clear_water_taken, floating_mud_taken, momentum_taken };
    }
    void add_liquid(const liquid& x) {
        clear_water += x.clear_water;
        floating_mud += x.floating_mud;
        momentum += x.momentum;
    }

    is_flow_possible_e check_if_flow_is_possible(const top_voxel_data& other) const {
        if (position[2]+ max_content_sum <= other.position[2] + 1)
            return other_too_high;
        if (other.position[2]+other.max_content_sum <= position[2] + 1)
            return other_too_low;
        return flow_possible;
    }

    is_flow_possible_e check_liquid_contact(const top_voxel_data& other) {
        const double this_lq_zmin = position[2] + mud_level;
        const double this_lq_zmax = total_water_level();
        const double other_lq_zmin = position[2] + mud_level;
        const double other_lq_zmax = total_water_level();
        if (this_lq_zmin > other_lq_zmax)
            return other_too_low;
        if (this_lq_zmax < other_lq_zmin)
            return other_too_high;
        return flow_possible;
    }

    bool is_sink() const {
        return _is_sink;
    }

    void take_all_liquid_from(top_voxel_data &from) {
        clear_water += from.clear_water;
        floating_mud += from.floating_mud;
        from.clear_water = from.floating_mud = 0.0;
    }

    void do_sedimentation_and_erosion(double flow, double sedimentation_coeff, double erosion_coeff) {
        //const double flow_velocity = flow / (total_liquid_amount());
        const double floating_mud_delta = (erosion_coeff * flow)//flow_velocity)
            - (sedimentation_coeff * floating_mud);
        floating_mud += floating_mud_delta;
        mud_level -= floating_mud_delta;
    }

    void suppress_momentum(double suppr_coeff) {
        momentum[2] *= 0.5;
        const double momentum_len = momentum.length();
        if (momentum_len == 0.0)
            return;
        momentum *= 2.0 * (sqrt(suppr_coeff*momentum_len + 1.0) - 1.0) / momentum_len;
    }
};

class indices_randomizer
{
public:
    void shuffled_indices(size_t seed, size_t n_elems, std::vector<size_t>& out) {
        indices.clear();
        indices.reserve(n_elems);
        for (size_t i = 0; i < n_elems; i++)
            indices.push_back(i);
        out.clear();
        out.reserve(n_elems);
        std::default_random_engine gen(seed);
        while (indices.size()) {
            std::uniform_int_distribution<size_t> index_gen(0, indices.size() - 1);
            size_t index_index = index_gen(gen);
            out.push_back(indices[index_index]);
            indices[index_index] = indices.back();
            indices.pop_back();
        }
    }
    std::vector<size_t> indices;
};

struct simulation_options {
    double erosion_coeff = 0.002; //erosion intensity
    double sedimentation_coeff = 0.01; //sedimentation intensity, 1.0=immediate mud deposition
    double flow_coeff = 0.2;// 0.125; //how much of water levels difference flows to target (0.5 will equalize water levels)
    double max_water_reduction_coeff = 0.4; //what fraction of source water can flow to target tile, max 0.5
    double max_slope = 5.0; //max water levels difference (limits flow with extreme levels difference)
    double momentum_suppr_coeff = 1.0;
    double momentum_flow_influence_coeff = 5.0;
};

class water_erosion {
public:

    water_erosion(const VoxelMap& voxels, const std::pair<double, double>& random_mud_min_max, unsigned int seed)
        : voxels(voxels)
        , _top_voxels_indices_by_xy(boost::extents[voxels.size[0] + 2][voxels.size[1] + 2])
        , seed(seed)
    {
        find_top_voxels();
        set_rainfall_flag_on_topmost_voxels();
        randomize_mud_on_top_voxels(random_mud_min_max.first, random_mud_min_max.second);
        create_sink_voxels_in_empty_columns();
        rebuild_all_nonsink_top_voxels_relations();
    }

    void simulation_step_rain(const simulation_options& opt, double rainfall) {
        for (top_voxel_data &voxel : top_voxels) {
            if (voxel.has_rainfall) {
                voxel.clear_water += rainfall;
            }
        }
        water_flow_step(opt);
    }

    void simulation_step_dry(const simulation_options& opt, double drying_speed) {
        for (top_voxel_data &voxel : top_voxels) {
            voxel.clear_water = std::max(0.0, voxel.clear_water - drying_speed);
        }
        water_flow_step(opt);
    }

    double calc_max_water_amount() const {
        double max_amount = 0.0;
        for (const top_voxel_data &voxel : top_voxels) {
            double voxel_amount = voxel.total_liquid_amount();
            if (voxel_amount > max_amount)
                max_amount = voxel_amount;
        }
        return max_amount;
    }

private:

    void water_flow_step(const simulation_options& opt) {
        shuff_gen.shuffled_indices(seed, top_voxels.size(), shuffled_indices);
        for (size_t index1 : shuffled_indices) {
            if (index1 >= top_voxels.size()) //can happen after removals
                continue;
            auto& voxel1 = top_voxels[index1];
            for (size_t index2 : voxel1.related_indices) {
                auto& voxel2 = top_voxels[index2];
                if (voxel1.is_sink() && voxel2.is_sink())
                    continue;
                const double level1 = voxel1.total_water_level();
                const double level2 = voxel2.total_water_level();
                const double fwd_clamped_levels_delta = std::min(std::max(-opt.max_slope, level2 - level1), opt.max_slope);
                const double fwd_x_delta = voxel2.position[0] - voxel1.position[0];
                const double fwd_y_delta = voxel2.position[1] - voxel1.position[1];
                const vec3<double> fwd_flow_vec = vec3<double>(fwd_x_delta, fwd_y_delta, fwd_clamped_levels_delta);
                const double flow_dir_coeff = opt.momentum_flow_influence_coeff *
                    vec3<double>::dotProduct(fwd_flow_vec.normalized(), (voxel1.momentum + voxel2.momentum)*0.5);

                const double fwd_max_flow = opt.flow_coeff*(flow_dir_coeff - fwd_clamped_levels_delta);

                const bool flow_fwd = fwd_max_flow > 0.0;
                auto& source_voxel = flow_fwd ? voxel1 : voxel2;
                auto& target_voxel = flow_fwd ? voxel2 : voxel1;
                assert(!source_voxel.is_sink());

                if (source_voxel.check_liquid_contact(target_voxel) == other_too_high) {
                    continue;
                }

                const auto flow_vec = fwd_flow_vec * (flow_fwd ? 1.0 : -1.0);
                const double flow =
                    std::min(std::min(std::abs(fwd_max_flow),
                        target_voxel.position[2] + target_voxel.max_content_sum - target_voxel.total_water_level()),
                        opt.max_water_reduction_coeff*source_voxel.total_liquid_amount());

                const auto flow_momentum = flow_vec*flow;

                source_voxel.do_sedimentation_and_erosion(flow, opt.sedimentation_coeff, opt.erosion_coeff);
                if (!(source_voxel.mud_level > -1.0 && source_voxel.mud_level < 2.0)) {
                    assert(source_voxel.mud_level > -1.0);
                    assert(source_voxel.mud_level < 2.0);
                }
                assert(source_voxel.clear_water >= 0.0);
                assert(source_voxel.floating_mud >= 0.0);
                const auto flow_liquid = source_voxel.take_liquid(flow);
                source_voxel.momentum += flow_momentum*0.5;
                source_voxel.suppress_momentum(opt.momentum_suppr_coeff);

                bool any_voxel_changed_position = false;
                if (source_voxel.mud_level < 0.0) {
                    on_voxel_completely_eroded(source_voxel.this_index);
                    any_voxel_changed_position = true;
                }
                if (source_voxel.mud_level > 1.0) {
                    on_voxel_completely_sedimented(source_voxel.this_index);
                    any_voxel_changed_position = true;
                }

                if (!target_voxel.is_sink()) {
                    target_voxel.do_sedimentation_and_erosion(flow, opt.sedimentation_coeff, opt.erosion_coeff);
                    target_voxel.add_liquid(flow_liquid);
                    target_voxel.momentum += flow_momentum*0.5;
                    target_voxel.suppress_momentum(opt.momentum_suppr_coeff);
                    assert(target_voxel.total_water_level() < target_voxel.position[2] + target_voxel.max_content_sum);
                    if (target_voxel.mud_level < 0.0) {
                        on_voxel_completely_eroded(target_voxel.this_index);
                        any_voxel_changed_position = true;
                    }
                    if (target_voxel.mud_level >= 1.0) {
                        on_voxel_completely_sedimented(target_voxel.this_index);
                        any_voxel_changed_position = true;
                    }
                }
                //If either this voxel was removed or moved and set of related voxels was changed,
                //we must break iteration over this voxel's neighbors and skip to next voxel.
                if (any_voxel_changed_position)
                    break;
            }
        }
    }

    //find voxels having empty space above
    void find_top_voxels() {
        for (int x = 0; x < voxels.size[0]; x++) {
            for (int y = 0; y < voxels.size[1]; y++) {
                int z = voxels.size[2] - 1;
                //first from top
                for (; z > 0; z--) {
                    if (voxels.data[x][y][z]) {
                        const size_t new_voxel_index = top_voxels.size();
                        top_voxels.emplace_back(new_voxel_index, vec3<int>({ x, y, z }));
                        voxels_indices_by_xy(x, y).push_back(new_voxel_index);
                        break;
                    }
                }
                //other voxels, but only those with air above them
                int air_voxels = 0;
                for (; z > 0; z--){
                    if(voxels.data[x][y][z]){
                        if (air_voxels == 0) //going through solid ground
                            continue;
                        const size_t new_voxel_index = top_voxels.size();
                        top_voxels.emplace_back(new_voxel_index, vec3<int>({ x, y, z }), air_voxels);
                        voxels_indices_by_xy(x, y).push_back(new_voxel_index);
                        air_voxels = 0;
                    }
                    else {
                        air_voxels++;
                    }
                }
            }
        }
    }

    //create water sinks (dummy voxels placed around the map which sink water)
    void create_sink_voxels_in_empty_columns() {
        for (int x = -1; x < (int)voxels.size[0] + 1; x++) {
            for (int y = -1; y < (int)voxels.size[1] + 1; y++) {
                if (voxels_indices_by_xy(x, y).size() == 0) {
                    const size_t new_voxel_index = top_voxels.size();
                    top_voxels.emplace_back(top_voxel_data::sink_flag(), new_voxel_index, x, y);
                    voxels_indices_by_xy(x, y).push_back(new_voxel_index);
                }
            }
        }
    }

    //find topmost voxel in each column and set its has_rainfall flag
    void set_rainfall_flag_on_topmost_voxels() {
        for (int x = 0; x < voxels.size[0]; x++) {
            for (int y = 0; y < voxels.size[1]; y++) {
                auto &top_list = voxels_indices_by_xy(x, y);
                if (top_list.size()) {
                    top_voxels[top_list.front()].has_rainfall = true;
                }
            }
        }
    }

    //randomize top voxels mud level - make flat surfaces less flat
    void randomize_mud_on_top_voxels(double mud_min, double mud_max) {
        std::default_random_engine gen(seed);
        std::uniform_real_distribution<double> mud_gen(mud_min, mud_max);
        for (top_voxel_data& voxel : top_voxels) {
            if(!voxel.is_sink())
                voxel.mud_level = mud_gen(gen);
        }
    }

    void rebuild_all_nonsink_top_voxels_relations() {
        for (top_voxel_data& voxel : top_voxels) {
            if (!voxel.is_sink())
                rebuild_top_voxel_data_relations(voxel.this_index);
        }
    }

    //remove all top voxels relations, its data from top_voxels and its index from list by x,y
    void remove_top_voxel_data(size_t index) {
        top_voxel_data& voxel = top_voxels[index];
        for (size_t related_index : voxel.related_indices) {
            top_voxels[related_index].related_indices.erase(index);
        }
        remove_voxel_index_from_list_by_xy(voxel.position[0], voxel.position[1], index);

        //move voxel from top_voxels.back() to removed voxel's index
        const top_voxel_data& back_voxel = top_voxels.back();
        const size_t old_back_voxel_index = back_voxel.this_index;
        top_voxels[index] = back_voxel;
        top_voxels.pop_back();
        voxel.this_index = index;
        remove_voxel_index_from_list_by_xy(voxel.position[0], voxel.position[1], old_back_voxel_index);
        add_voxel_index_to_list_by_xy(voxel.position[0], voxel.position[1], index);
        for (size_t related_index : voxel.related_indices) {
            top_voxels[related_index].related_indices.erase(old_back_voxel_index);
            top_voxels[related_index].related_indices.insert(index);
        }
    };

    void rebuild_top_voxel_data_relations(size_t index) {
        auto& voxel = top_voxels[index];
        for (const size_t related_index : voxel.related_indices) {
            top_voxels[related_index].related_indices.erase(index);
        }
        voxel.related_indices.clear();
        for (const vec3<int> &delta : {
            vec3<int>({ 1, 0, 0 }), vec3<int>({ -1, 0, 0 }),
            vec3<int>({ 0, 1, 0 }), vec3<int>({ 0, -1, 0 })
        }) {
            const vec3<int> neigh_pos = voxel.position + delta;
            for (const size_t neigh_index : voxels_indices_by_xy(neigh_pos[0], neigh_pos[1])) {
                auto& neigh = top_voxels[neigh_index];
                switch (voxel.check_if_flow_is_possible(neigh)) {
                case other_too_high:
                    continue;
                case flow_possible:
                    voxel.related_indices.insert(neigh_index);
                    neigh.related_indices.insert(index);
                    continue;
                case other_too_low:
                    break;
                }
                break;
            }
        }
    };

    void on_voxel_completely_eroded(size_t index) {
        top_voxel_data& voxel = top_voxels[index];
        assert(!voxel.is_sink());
        const auto &pos = voxel.position;
        voxels[pos] = false;
        if (pos[2] > 0 && voxels.data[pos[0]][pos[1]][pos[2] - 1]) {
            //there is more gorund below
            voxel.mud_level += 1.0;
            voxel.position[2] -= 1;
            rebuild_top_voxel_data_relations(index);
        }
        else {
            //if there is a top voxel below
            for (size_t same_xy_top_voxel_i : voxels_indices_by_xy(pos[0], pos[1])) {
                if (top_voxels[same_xy_top_voxel_i].position[2] < pos[2]) {
                    //merge with that voxel
                    merge_top_voxel_data(same_xy_top_voxel_i, index);
                    return;
                }
            }
            //else make this a sink
            voxel = top_voxel_data(top_voxel_data::sink_flag(), index, voxel.position[0], voxel.position[1]);
            rebuild_top_voxel_data_relations(index);
        }
    };

    //merge top_voxel with index2 with top voxel with index1
    //used when top one's ground completely erodes
    void merge_top_voxel_data(size_t index_to, size_t index_from) {
        top_voxel_data& voxel_to = top_voxels[index_to];
        top_voxel_data& voxel_from = top_voxels[index_from];
        assert(!voxel_from.is_sink());
        if (!voxel_to.is_sink()) {
            voxel_to.mud_level += voxel_from.mud_level;
            voxel_to.take_all_liquid_from(voxel_from);
            voxel_to.has_rainfall |= voxel_from.has_rainfall;
        }
        remove_top_voxel_data(index_from);
        rebuild_top_voxel_data_relations(index_to);
    };

    void on_voxel_completely_sedimented(size_t index) {
        top_voxel_data& voxel = top_voxels[index];
        assert(!voxel.is_sink());
        const auto &pos = voxel.position;
        if (pos[2] == voxels.size[2] - 1) {
            //reached simulation box ceiling - cannot grow further
            voxel.mud_level = 1.0;
            return;
        }
        else {
            if (!voxels.data[pos[0]][pos[1]][pos[2] + 1]) {
                //there is nothing above
                voxel.position[2] += 1;
                voxels[voxel.position] = true;
                voxel.mud_level -= 1.0;
                rebuild_top_voxel_data_relations(index);
            }
            else {
                auto& same_xy_voxels = voxels_indices_by_xy(pos[0], pos[1]);
                for (auto it = same_xy_voxels.rbegin(); it != same_xy_voxels.rend(); ++it) {
                    if (top_voxels[*it].position[2] > pos[2]) {
                        merge_top_voxel_data(*it, index);
                        return;
                    }
                }
                //impossible - voxel sedimented to the ceiling, so there must be data for a top voxel above it
                assert(false);
            }
        }
    };

    void add_voxel_index_to_list_by_xy(int x, int y, size_t index) {
        std::list<size_t>& l = voxels_indices_by_xy(x, y);
        if (l.size() == 0) {
            l.push_back(index);
        }
        else {
            const int voxel_z = top_voxels[index].position[2];
            for (auto it = l.begin(); it != l.end(); ++it) {
                const top_voxel_data& other = top_voxels[*it];
                if (voxel_z > other.position[2]) {
                    l.insert(it, index);
                    return;
                }
            }
            //lower z than of all other top voxels at this x,y
            l.push_back(index);
        }
    }
    void remove_voxel_index_from_list_by_xy(int x, int y, size_t index) {
        auto &indices_at_pos = voxels_indices_by_xy(x, y);
        for (auto it = indices_at_pos.begin(); it != indices_at_pos.end(); ++it) {
            if (*it == index) {
                indices_at_pos.erase(it);
                break;
            }
        }
    }
    std::list<size_t>& voxels_indices_by_xy(int x, int y) {
        return _top_voxels_indices_by_xy[x + 1][y + 1];
    };

    VoxelMap voxels;
    std::vector<top_voxel_data> top_voxels;
    boost::multi_array<std::list<size_t>, 2> _top_voxels_indices_by_xy;
    indices_randomizer shuff_gen;
    std::vector<size_t> shuffled_indices;
    unsigned int seed;
};

}


template <typename value_t>
void set_value_if_in_map(value_t& value, const std::map<std::string, std::string>& map, const std::string& name) {
    auto it = map.find(name);
    if (it != map.end())
        value = boost::lexical_cast<value_t>(it->second);
}

void genWater(const int(&size)[3], int *const *const *data, const std::map<std::string, std::string>& options) {
    voxel1::simulation_options opt;
    set_value_if_in_map(opt.flow_coeff, options, "flow_intensity");
    set_value_if_in_map(opt.erosion_coeff, options, "erosion_intensity");
    set_value_if_in_map(opt.sedimentation_coeff, options, "sedimentation_intensity");
    set_value_if_in_map(opt.momentum_flow_influence_coeff, options, "water_momentum");
    size_t rain_iterations = 200;
    double rain_intensity = 0.1;
    size_t dry_iterations = 100;
    double drying_speed = 0.1;
    unsigned int seed = 1;// time(NULL);
    set_value_if_in_map(seed, options, "seed");
    set_value_if_in_map(rain_iterations, options, "rain_iterations");
    set_value_if_in_map(rain_intensity, options, "rain_intensity");
    set_value_if_in_map(dry_iterations, options, "dry_iterations");
    set_value_if_in_map(drying_speed, options, "drying_speed");
    voxel1::water_erosion gen(voxel1::VoxelMap(size, data), std::make_pair(0.25, 0.75), seed);
    for (size_t iteration = 0; iteration < rain_iterations; iteration++) {
        gen.simulation_step_rain(opt, rain_intensity);
    }
    for (size_t iteration = 0; iteration < dry_iterations; iteration++) {
        gen.simulation_step_dry(opt, drying_speed);
    }
}
