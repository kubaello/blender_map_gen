#pragma once
#include <map>
#include <string>

void genMountains(const int (&size)[3], int *const *const * data, const std::map<std::string, std::string>& options);
void genWater(const int(&size)[3], int *const *const * data, const std::map<std::string, std::string>& options);
void genCaves(int size[3],int ***data,std::map<std::string,std::string>& options);
