#include <cassert>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <algorithm>
#include <iostream>

#include <boost/lexical_cast.hpp>

#include "generators.hpp"


int log2_round_up(int x) {
    assert(x > 0);
    int log = 0;
    x -= 1;
    while (x > 0) {
        x >>= 1;
        log++;
    }
    return log;
}

class Fractal
{
public:
    Fractal(int size_exponent, float hmin, float hmax);
    ~Fractal();
    
    int *const *const generate(unsigned int seed);

private:
    int size;  //size, must be a power of 2
    int size_exponent;
    float hmin; //Max and min size added to midpoint
    float hmax;
    int **heights;
    
    void midpoint(int i, int start, int topx, int topy);
};

template <typename value_t>
value_t get_with_default(const std::map<std::string, std::string>& map, const std::string& name, value_t default_value) {
    auto it = map.find(name);
    if (it != map.end())
        return boost::lexical_cast<value_t>(it->second);
    else
        return default_value;
}

void genMountains(const int (&size)[3], int *const *const * data, const std::map<std::string, std::string>& options) {
    const float ground_level = get_with_default<float>(options, "groundLevelFactor", 0.5)*(size[2]-1);
    const float min_level = get_with_default<float>(options, "minGroundLevelFactor", 0.125)*(size[2]-1);
    const float max_level = get_with_default<float>(options, "maxGroundLevelFactor", 0.875)*(size[2]-1);
    unsigned int seed = get_with_default<unsigned int>(options, "seed", time(NULL));
    assert(0 <= min_level && min_level <= ground_level && ground_level <= max_level);
    
    const int exponent = log2_round_up(1+std::max(size[0], size[1]));
    Fractal gen(exponent, min_level-ground_level, max_level-ground_level);
    int *const *const heights = gen.generate(seed);
    for(int x=0; x<size[0]; x++) {
        for(int y=0; y<size[1]; y++) {
            const int height = heights[x+1][y+1] + ground_level;
            for(int z=0; z<std::min(height, size[2]); z++) {
                data[x][y][z] = 1;
            }
        }
    }
}

Fractal::Fractal(int size_exponent, float hmin, float hmax)
    : size((1<<size_exponent) + 1)
    , size_exponent(size_exponent)
    , hmin(hmin)
    , hmax(hmax)
{
    heights = new int*[size];
    for(int x=0;x<size;x++){
        heights[x] = new int[size];
        for(int y=0;y<size;y++) {
            heights[x][y] = 0;
        }
    }
}

Fractal::~Fractal() {
    for(int x=0;x<size;x++){
        delete[] heights[x];
    }
    delete[] heights;
}

int *const *const  Fractal::generate(unsigned int seed) {
    srand(seed);
    midpoint(size_exponent, size_exponent, 0, 0);
    return heights;
}

//recursive algorithm
/*i - changable number of iterations e.g. 5 or 6
 *start - number of iterations
 **/
void Fractal::midpoint(const int i, const int start, const int topx, const int topy) {
    if(i<1) {
        return;
    }

    //find powerght for this section
    const int power = 1<<i;

    //Calculate midpoints
    
    //Top middle
    const int x1 = topx + power/2;
    const int y1 = topy;

    //Right middle
    const int x2 = topx + power;
    const int y2 = topy + power/2;

    //Bottom middle
    const int x3 = x1;
    const int y3 = y1 + power;

    //Left Middle
    const int x4 = x2 - power;
    const int y4 = y2;

    //Midpoint
    const int xmid = x1;
    const int ymid = y2;

    //Calculate the values of the midpoints

    //Top middle
    heights[x1][y1] = heights[topx][topy] + (heights[topx+power][topy] - heights[topx][topy])/2;

    //Right middle
    heights[x2][y2] = heights[topx+power][topy] + (heights[topx+power][topy+power] - heights[topx+power][topy])/2;

    //Bottom middle
    heights[x3][y3] = heights[topx][topy+power] + (heights[topx+power][topy+power] - heights[topx][topy+power])/2;

    //Left middle
    heights[x4][y4] = heights[topx][topy] + (heights[topx][topy+power] - heights[topx][topy])/2;

    //Midpoint
    heights[xmid][ymid] = heights[x1][y1] + (heights[x3][y3] - heights[x1][y1])/2;


    //As iterations go foward decrease range of randomness
    const float min = hmin * ((float)i / (float)start);
    const float max = hmax * ((float)i / (float)start);

    //Add random value to midpoint (from hmin to hmax)
    heights[xmid][ymid] +=  (int)(min + ((float)rand() / (float) RAND_MAX) * (max - min));


    //Add less variance of random heights when mountians are larger - decrease i
    //Recursive steps

    //Upper left quadrant
    midpoint(i-1, start, topx, topy);

    //Upper right quadrant
    midpoint(i-1, start, x1, y1);

    //Lower left quadrant
    midpoint(i-1, start, x4, y4);

    //Lower right quadrant
    midpoint(i-1, start, xmid, ymid);
}
