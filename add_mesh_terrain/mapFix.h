void fixPoint(int x,int y,int z,int*** data){
    for(int dx=-1;dx<=1;++dx)
        for(int dy=-1;dy<=1;++dy)
            for(int dz=-1;dz<=1;++dz){
                if(abs(dx)+abs(dy)+abs(dz)!=2)
                    continue;
                int x1=x+dx;
                int y1=y+dy;
                int z1=z+dz;
                if(data[x][y][z]==0 && data[x1][y1][z1]==0){
                    if(dz==0){
                        if(data[x1][y][z]==1 && data[x][y1][z]==1){
                            data[x1][y][z]=0;
                            data[x][y1][z]=0;
                        }
                    }
                    if(dx==0){
                        if(data[x][y][z1]==1 && data[x][y1][z]==1){
                            data[x][y][z1]=0;
                            data[x][y1][z]=0;
                        }
                    }
                    if(dy==0){
                        if(data[x][y][z1]==1 && data[x1][y][z]==1){
                            data[x][y][z1]=0;
                            data[x1][y][z]=0;
                        }
                    }
                }
            }
}

// remove parts with 0 thickness
void makeFix(int size[3],int*** data){
    for(int q=0;q<3;++q)
        for(int x=1;x<size[0]-1;++x)
            for(int y=1;y<size[1]-1;++y)
                for(int z=1;z<size[2]-1;++z)
                    fixPoint(x,y,z,data);
}
