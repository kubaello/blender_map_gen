#include <QVector3D>
#include <cstdlib>
#include <vector>
#include <queue>
using namespace std;


namespace caves{

class CaveNode{
    typedef int Voxel;
    QVector3D pos;
    QVector3D velocity; /// to avoid rapid turns
    float scale;
    CaveNode* parent;

    vector<CaveNode*> children;
public:
    /// global map data
    static Voxel ***vdata;
    static int *vsize;
    static float lengthFactor;

    CaveNode(const QVector3D& po, const QVector3D& vel=QVector3D(0,0,0), float sc=1.0, CaveNode* par=NULL)
        :pos(po), velocity(vel), scale(sc), parent(par){}

    bool insideMap(const QVector3D& v){
        return v[0]>=0 && v[1]>=0 && v[2]>=0 
            && v[0]<vsize[0] && v[1]<vsize[1] && v[2]<vsize[2];
    }
    bool insideMap(const QVector3D& v,int b){
        return v[0]>=b && v[1]>=b && v[2]>=b 
            && v[0]<=vsize[0]-b && v[1]<=vsize[1]-b && v[2]<=vsize[2];
    }

    /// make next segment and continue with probability prob
    void makeBranch(float prob);

    void applyToData();
};

}
