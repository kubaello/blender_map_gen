#pragma once

#include <cassert>
#include <array>

namespace voxel1 {

template <typename value_t_>
class my_vec3 {
public:
    typedef value_t_ value_t;
    my_vec3(std::initializer_list<value_t> l)
        : data()
    {
        assert(l.size() == 3);
        size_t i = 0;
        for (value_t x : l)
            data[i++] = x;
    }
    my_vec3(const std::array<value_t, 3>& data)
        : data(data)
    { }
    my_vec3(value_t x, value_t y, value_t z)
        : data({x, y, z})
    { }
    value_t& operator[](size_t i) {
        return data[i];
    }
    const value_t& operator[](size_t i) const {
        return data[i];
    }
    my_vec3 operator+(const my_vec3& other) const {
        return my_vec3({ (*this)[0] + other[0], (*this)[1] + other[1], (*this)[2] + other[2] });
    }
    my_vec3& operator+=(const my_vec3& other) {
        (*this)[0] += other[0];
        (*this)[1] += other[1];
        (*this)[2] += other[2];
        return *this;
    }
    my_vec3 operator-(const my_vec3& other) const {
        return my_vec3({ (*this)[0] - other[0], (*this)[1] - other[1], (*this)[2] - other[2] });
    }
    my_vec3& operator-=(const my_vec3& other) {
        (*this)[0] -= other[0];
        (*this)[1] -= other[1];
        (*this)[2] -= other[2];
        return *this;
    }
    my_vec3 operator*(value_t val) const {
        return my_vec3({ (*this)[0] * val, (*this)[1] * val, (*this)[2] * val });
    }
    my_vec3& operator*=(value_t val) {
        (*this)[0] *= val;
        (*this)[1] *= val;
        (*this)[2] *= val;
        return *this;
    }
    my_vec3 operator/(value_t val) const {
        return my_vec3({ (*this)[0] / val, (*this)[1] / val, (*this)[2] / val });
    }
    my_vec3& operator/=(value_t val) {
        (*this)[0] /= val;
        (*this)[1] /= val;
        (*this)[2] /= val;
        return *this;
    }
    value_t dot(const my_vec3& other) const {
        return (*this)[0] * other[0] + (*this)[1] * other[1] + (*this)[2] * other[2];
    }
    static value_t dotProduct(const my_vec3& v1, const my_vec3& v2) {
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
    }
    value_t length() const {
        return sqrt(dot(*this));
    }
    my_vec3 normalized() const {
        return my_vec3(*this) / length();
    }
    constexpr size_t size() const {
        return 3;
    }
    std::array<value_t, 3> data;
};

template <class T>
using vec3 = my_vec3<T>;

class VoxelMap
{
public:
    VoxelMap(const int (&size)[3], int *const *const * data)
        : size(size)
        , data(data)
    {
    }

    int& operator[] (const vec3<size_t>& position) {
        return data[position[0]][position[1]][position[2]];
    }
    int& operator[] (const vec3<int>& position) {
        return data[position[0]][position[1]][position[2]];
    }

    bool is_valid_position(int x, int y, int z) const {
        return x > 0 && x < size[0] && y>0 && y < size[1] && z>0 && z < size[2];
    }

    const int *size;
    int *const *const * data;
};
}
