#include "caves.h"
#include <cstdlib>
#include "generators.hpp"
using namespace std;

namespace caves{

typedef int Voxel;
Voxel*** CaveNode::vdata=NULL;
int* CaveNode::vsize=NULL;
float CaveNode::lengthFactor=0.9;

void CaveNode::makeBranch(float prob){
    QVector3D newNode;

    while(1){
        newNode=QVector3D( std::rand()%10 - 5,
                            std::rand()%10 - 5,
                            -(std::rand()%10) );
        newNode[2]/=2; /// make tunnels more horizontal
        newNode.normalize();

        /// no rapid turns
        newNode*=2.3;
        newNode+=velocity;
        QVector3D newVelocity=newNode.normalized();

        newNode.normalize();
        newNode*=std::rand()%7+3;
        newNode+=pos;

        /// not branch in air
        if(!insideMap(newNode,3)){
            scale/=2;
            return;
        }
        if(vdata[(int)newNode[0]][(int)newNode[1]][(int)newNode[2]]==0)
            return;

        float newScale=scale*prob*(1000+(rand()%400)-200)*0.001;
        children.push_back(new CaveNode(newNode,newVelocity,newScale,this));


        /// go deeper
        if(newScale>0.4 && ((float)(std::rand()%1000)/1000)<prob)
            (*children.rbegin())->makeBranch(prob*lengthFactor);

        /// stop branching this node
        if(((float)(std::rand()%1000)/1000)>prob/2.5)
            break;
    }

}

void CaveNode::applyToData(){
    /// BFS (could be recursive as well)
    queue<CaveNode*> q;
    q.push(this);
    while(!q.empty()){
        CaveNode* c=q.front();
        q.pop();
        if(c->parent!=NULL){
            QVector3D d=c->parent->pos - c->pos;
            for(int x=0;x<vsize[0];++x){
                for(int y=0;y<vsize[1];++y){
                    for(int z=0;z<vsize[2];++z){
                        QVector3D v(x,y,z);
                        QVector3D d1=c->pos-v;
                        QVector3D d2=c->parent->pos-v;

                        float r=3;
                        if(d1.length()<r*c->parent->scale)
                            vdata[x][y][z]=0;
                        if(d2.length()<r*c->scale)
                            vdata[x][y][z]=0;
                        if(v.distanceToLine(c->pos,d.normalized()) < r*(c->parent->scale+c->scale)/2
                            && max(d1.length(),d2.length())<d.length())
                            vdata[x][y][z]=0;
                    }
                }
            }
        }
        for(auto a:c->children)
            q.push(a);
    }
}

}

void genCaves(int size[3],int ***data,map<string,string>& options){
    caves::CaveNode::vdata=data;
    caves::CaveNode::vsize=size;
    caves::CaveNode::lengthFactor=stof(options["cavesLengthFactor"]);
    std::srand(stoi(options["seed"]));
    int cavesCount=stoi(options["maxCavesCount"]);
    for(int a=0;a<cavesCount;++a){
        int x=rand()%size[0];
        int y=rand()%size[1];

        int z=size[2]-1;
        while(z>0 && data[x][y][z]==0)
            --z;

        caves::CaveNode n(QVector3D(x,y,z));
        n.makeBranch(1.0);

        n.applyToData();
    }
}

