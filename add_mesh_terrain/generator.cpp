#include <iostream>
#include <map>
#include <string>

#include "generators.hpp"

using namespace std;

#include "mapFix.h"

int main(){
    map<string, string> options;
    int n;
    cin>>n;
    for (int a=0;a<n;++a) {
        string key, value;
        cin >> key >> value;
        options[key] = value;
    }

    int size[3]={stoi(options["sizeX"]), stoi(options["sizeY"]), stoi(options["sizeZ"])};

    int ***data=new int**[size[0]];
    for(int x=0;x<size[0];++x){
        data[x]=new int*[size[1]];
        for(int y=0;y<size[1];++y){
            data[x][y]=new int[size[2]];
            for(int z=0;z<size[2];++z)
                data[x][y][z]=0;
        }
    }

    if(options["enableMountains"]=="True")
        genMountains(size, data, options);
    else{
        /// fill with 1
        for(int x=0;x<size[0];++x)
            for(int y=0;y<size[1];++y)
                for(int z=0;z<size[2];++z)
                    data[x][y][z]=1;
    }
    if(options["enableWater"]=="True")
        genWater(size, data, options);
    if(options["enableCaves"]=="True")
        genCaves(size,data,options);
    makeFix(size,data);

    if (options.count("output_format") == 0) {
        options["output_format"] = "01";
    }
    if (options["output_format"] == "01") {
        for (int x = 0; x < size[0]; ++x)
            for (int y = 0; y < size[1]; ++y)
                for (int z = 0; z < size[2]; ++z)
                    cout << data[x][y][z] << " ";
    }
    else if (options["output_format"] == ".#") {
        cout << size[0] << ' ' << size[1] << ' ' << size[2] << endl << endl;
        for (int z = 0; z < size[2]; z++) {
            for (int y = 0; y < size[1]; y++) {
                for (int x = 0; x < size[0]; x++) {
                    cout << (data[x][y][z] ? '#' : '.');
                }
                cout << '\n';
            }
            cout << '\n';
        }
    }
    else {
        cerr << "Invalid output format '" << options["output_format"] << "'\n";
        return 1;
    }

    return 0;
}
