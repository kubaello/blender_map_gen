bl_info = {
    "name": "Terrain Generaor",
    "author": "",
    "category": "Add Mesh",
}

import bpy
import math
import subprocess
import os


def inside(x,y,z,size):
    return x>=0 and y>=0 and z>=0 and x<size[0] and y<size[1]  and z<size[2]

def addSquare(x,y,z,a,b,c,verts,faces):
    step=1.0
    x=float(x)*step
    y=float(y)*step
    z=float(z)*step
    if(a!=0):
        if a==-1:
            a=0
        verts+=[(x+a*step,y,z)]
        verts+=[(x+a*step,y+step,z)]
        verts+=[(x+a*step,y+step,z+step)]
        verts+=[(x+a*step,y,z+step)]
        faces+=[(len(verts)-4,len(verts)-3,len(verts)-2,len(verts)-1)]
    if(b!=0):
        if b==-1:
            b=0
        verts+=[(x,y+b*step,z)]
        verts+=[(x+step,y+b*step,z)]
        verts+=[(x+step,y+b*step,z+step)]
        verts+=[(x,y+b*step,z+step)]
        faces+=[(len(verts)-4,len(verts)-3,len(verts)-2,len(verts)-1)]
    if(c!=0):
        if c==-1:
            c=0
        verts+=[(x,y,z+c*step)]
        verts+=[(x+step,y,z+c*step)]
        verts+=[(x+step,y+step,z+c*step)]
        verts+=[(x,y+step,z+c*step)]
        faces+=[(len(verts)-4,len(verts)-3,len(verts)-2,len(verts)-1)]

# @returns  (vertices,edges,faces)
def makeMesh(data,size):
    verts=[]
    faces=[]
    for x in range(size[0]):
        for y in range(size[1]):
            for z in range(size[2]):
                if data[x][y][z]!=0:
                    for a in [-1,1]:
                        if inside(x+a,y,z,size)==False or data[x+a][y][z]==0:
                            addSquare(x,y,z,a,0,0,verts,faces)
                    for a in [-1,1]:
                        if inside(x,y+a,z,size)==False or data[x][y+a][z]==0:
                            addSquare(x,y,z,0,a,0,verts,faces)
                    for a in [-1,1]:
                        if inside(x,y,z+a,size)==False or data[x][y][z+a]==0:
                            addSquare(x,y,z,0,0,a,verts,faces)
    return verts, [], faces


class ObjectAddTerrain(bpy.types.Operator):

    bl_idname = "object.terrain_add"
    bl_label = "Add Terrain"
    bl_options = {'REGISTER', 'UNDO', 'PRESET'}
    bl_description = "Add random terrain"

    # properties

    # update mesh when changed properties
    autoUpdate = bpy.props.BoolProperty(name="Mesh update",
                default=True,
                description="Update mesh")

    seed = bpy.props.IntProperty(name="Seed",
                default=1,
                description="Random seed")

    size = bpy.props.IntVectorProperty(name="Size",
                default=(40,40,30),
                min=1,
                size=3)

    enableRemesh = bpy.props.BoolProperty(name="Enable Remesh",
                default=True,
                description="Add remesh modifier to object")

    # mountains properties
    enableMountains = bpy.props.BoolProperty(name="Enable Mountains",
                default=True,
                description="Generate Mountains")
    groundLevelFactor = bpy.props.FloatProperty(name="Ground Level", default=0.5,
                min=0,
                max=0.999)
    minGroundLevelFactor = bpy.props.FloatProperty(name="Minimal Ground Level", default=0.0,
                min=0,
                max=0.999) # crashes if bigger
    maxGroundLevelFactor = bpy.props.FloatProperty(name="Maximal Ground Level", default=0.999,
                min=0,
                max=0.999) # crashes if bigger

    # water properties
    enableWater = bpy.props.BoolProperty(name="Enable Water",
                default=True,
                description="Generate Water")
    rainIterations = bpy.props.IntProperty(name="Rain iterations",
                default=20,
                min=0,
                description="")
    rainIntensity = bpy.props.FloatProperty(name="Rain intensity",
                default=0.05,
                max=0.5,
                description="")
    dryIterations = bpy.props.IntProperty(name="Dry iterations",
                default=50,
                min=0,
                description="")
    dryingSpeed = bpy.props.FloatProperty(name="Drying speed",
                default=0.3,
                max=0.5,
                description="")
    erosionIntensity = bpy.props.FloatProperty(name="Erosion intensity",
                default=0.18,
                min=0.0,
                max=0.3,
                description="How quickly water erodes terrain")
    sedimentationIntensity = bpy.props.FloatProperty(name="Sedimentation intensity",
                default=0.07,
                min=0.0,
                max=1.0,
                description="How quickly eroded terrain deposits")

    # caves properties
    enableCaves = bpy.props.BoolProperty(name="Enable caves",
                default=True,
                description="Enable generating caves")
    maxCavesCount = bpy.props.IntProperty(name="MaxCavesCount",
                default=6,
                min=5,
                description="Max number of caves")
    cavesLengthFactor = bpy.props.FloatProperty(name="Caves length factor",
                default=0.9,
                min=0.0,
                max=0.98,
                description="Number that probability of continuing cave is multipled by every step")




    def execute(self, context):
        def make_exec_path():
            suffix = '.exe' if os.name == 'nt' else ''
            return os.path.join(os.path.dirname(__file__), 'generator'+suffix)

        # deselect all objects when in object mode
        if bpy.ops.object.select_all.poll():
            bpy.ops.object.select_all(action='DESELECT')

        if self.autoUpdate:
            options={}
            options['seed']=self.seed
            options['sizeX']=self.size[0]
            options['sizeY']=self.size[1]
            options['sizeZ']=self.size[2]

            # mountains
            options['enableMountains']=self.enableMountains
            options['maxGroundLevelFactor']=self.maxGroundLevelFactor
            if(self.groundLevelFactor>self.maxGroundLevelFactor*0.9):
                self.groundLevelFactor=self.maxGroundLevelFactor*0.9
            options['groundLevelFactor']=self.groundLevelFactor
            if(self.minGroundLevelFactor>self.groundLevelFactor*0.9):
                self.minGroundLevelFactor=self.groundLevelFactor*0.9
            options['minGroundLevelFactor']=self.minGroundLevelFactor

            # water
            options['enableWater']=self.enableWater
            options['rain_iterations']=self.rainIterations
            options['rain_intensity']=self.rainIntensity
            options['dry_iterations']=self.dryIterations
            options['drying_speed']=self.dryingSpeed
            options['erosion_intensity']=self.erosionIntensity
            options['sedimentation_intensity']=self.sedimentationIntensity

            # caves
            options['enableCaves']=self.enableCaves
            options['maxCavesCount']=self.maxCavesCount
            options['cavesLengthFactor']=self.cavesLengthFactor

            inp=str(len(options))+'\n'
            inp+=''.join([str(k)+' '+str(v)+' ' for k,v in options.items()])
            execPath=make_exec_path()
            process=subprocess.Popen(execPath,stdin=subprocess.PIPE,stdout=subprocess.PIPE, shell=True )
            outData=process.communicate(input=inp.encode())[0]
            outData=outData.decode()
            outData=outData.split(' ')
            outData=[int(a) for a in outData if (a=='1' or a=='0') ]

            size=self.size


            # rewrite outData to 3D data array
            data=[[[0 for a in range(size[2])] for b in range(size[1])] for c in range(size[0])]
            i=0
            for x in range(size[0]):
                for y in range(size[1]):
                    for z in range(size[2]):
                        data[x][y][z]=outData[i]
                        i+=1

            # make blender mesh
            mesh = bpy.data.meshes.new("TerrainMesh")
            mesh.from_pydata(*makeMesh(data,size))
            mesh.update()

            object = bpy.data.objects.new("Terrain", mesh)
            object.data = mesh

            scene = bpy.context.scene
            scene.objects.link(object)
            object.select = True
            scene.objects.active = object

            if self.enableRemesh:
                bpy.ops.object.modifier_add(type='REMESH')
                bpy.context.object.modifiers["Remesh"].mode = 'SMOOTH'
                bpy.context.object.modifiers["Remesh"].scale = 0.99
                bpy.context.object.modifiers["Remesh"].octree_depth = round(math.log(max(*self.size), 2))


        else:
            return {'PASS_THROUGH'}

        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout

        box = layout.box()
        box.prop(self, 'autoUpdate')
        box.prop(self, 'enableRemesh')
        box.prop(self, 'seed')
        box.prop(self, 'size')

        box = layout.box()
        box.prop(self, 'enableMountains')
        if self.enableMountains:
            box.prop(self, 'groundLevelFactor')
            box.prop(self, 'minGroundLevelFactor')
            box.prop(self, 'maxGroundLevelFactor')

        box = layout.box()
        box.prop(self, 'enableWater')
        if self.enableWater:
            box.prop(self, 'rainIterations')
            box.prop(self, 'rainIntensity')
            box.prop(self, 'dryIterations')
            box.prop(self, 'dryingSpeed')
            box.prop(self, 'erosionIntensity')
            box.prop(self, 'sedimentationIntensity')

        box = layout.box()
        box.prop(self, 'enableCaves')
        if self.enableCaves:
            box.prop(self, 'maxCavesCount')
            box.prop(self, 'cavesLengthFactor')



def menu_func_terrain(self, context):
    self.layout.operator(ObjectAddTerrain.bl_idname, text="Terrain", icon="PLUGIN")

def register():
    bpy.utils.register_class(ObjectAddTerrain)
    bpy.types.INFO_MT_mesh_add.append(menu_func_terrain)

def unregister():
    bpy.utils.unregister_class(ObjectAddTerrain)
    bpy.types.INFO_MT_mesh_add.remove(menu_func_terrain)


#def register():
#    bpy.utils.register_module(__name__)
#
#
#def unregister():
#    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
