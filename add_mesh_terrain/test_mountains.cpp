#include <iostream>
#include "generators.hpp"


using std::cout;
using std::endl;
using std::map;
using std::string;

int main() {    
    int map_size[3] = { 60, 60, 40 };

    //alloc map arrays
    int ***map_data = new int**[map_size[0]];
    for (size_t x = 0; x < map_size[0]; x++) {
        map_data[x] = new int*[map_size[1]];
        for (size_t y = 0; y < map_size[1]; y++) {
            map_data[x][y] = new int[map_size[2]];
                for (size_t z = 0; z < map_size[2]; z++)
                    map_data[x][y][z] = false;
        }
    }
    
    genMountains(map_size, map_data, map<string, string>());
    // output map layers
    cout << map_size[0] << ' ' << map_size[1] << ' ' << map_size[2] << endl;
    for (int z = 0; z < map_size[2]; z++) {
        for (int y = 0; y < map_size[1]; y++) {
            for (int x = 0; x < map_size[0]; x++) {
                cout << (map_data[x][y][z] ? '#' : '.');
            }
            cout << '\n';
        }
        cout << '\n';
    }
    return 0;
}
