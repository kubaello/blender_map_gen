QT+= gui
CONFIG+= c++11

TARGET= add_mesh_terrain/generator

SOURCES+= add_mesh_terrain/generator.cpp \
          add_mesh_terrain/mountains.cpp \
          add_mesh_terrain/water.cpp \
          add_mesh_terrain/caves.cpp 

HEADERS+= add_mesh_terrain/generators.hpp \
          add_mesh_terrain/voxel_map.hpp \
          add_mesh_terrain/mapFix.h 
